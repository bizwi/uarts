Bizwi for MIFI
make -j4 image PROFILE=Default PACKAGES="kmod-usb-net kmod-usb-uhci kmod-usb-ohci kmod-usb-serial python python-openssl \
kmod-ledtrig-usbdev kmod-ledtrig-netdev kmod-leds-gpio wget libpcre librt libc libpthread libopenssl libpthread mtd \
wshaper -ip6tables -6relayd -odhcp6c kmod-input-core kmod-hid kmod-eeprom-93cx6 kmod-usb-serial-wwan kmod-gpio-button-hotplug kmod-usb-hid kmod-cfg80211 kmod-mac80211 kmod-rt2800-lib kmod-rt2800-mmio kmod-rt2800-soc kmod-rt2800-usb \
kmod-rt2x00-lib kmod-rt2x00-mmio kmod-slhc kmod-tun usbutils pciutils iw jshn jsonfilter wireless-tools hostapd hostapd-common nodogsplash fstools libffi openssh-server dnsmasq dropbear \
openssh-client iptables-mod-filter kmod-ipt-conntrack kmod-ipt-conntrack-extra iptables-mod-extra iptables-mod-conntrack-extra iptables-mod-ipopt iptables-mod-nat-extra \
wpa-supplicant luci luci-app-firewall luci-base luci-lib-nixio luci-mod-admin-full luci-proto-3g luci-proto-ppp luci-theme-bootstrap luci-theme-freifunk-bno luci-theme-freifunk-generic luci-theme-openwrt" \
FILES=bizwi-config/mifi
