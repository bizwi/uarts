#!/bin/sh
# install
# jcb/BIZWI 14/10/14
# First time installer, called only once when routers boot, using bizwi Image, with basic sensor cloud register capabilities.
#
# ChangeLog
# v0.7
# -deleted network/wireless configuration based on router model. Now, network/wireless config should be universal (all models suported)
#

EXPR=/usr/bin/expr
GREP=/bin/egrep
MD5SUM=/usr/bin/md5sum
IFCONFIG=/sbin/ifconfig
AWK=/usr/bin/awk
TR=/usr/bin/tr
SED=/bin/sed
LOGGER=/usr/bin/logger
LOGREAD=/sbin/logread
REBOOT=/sbin/reboot
WGET=/usr/bin/wget
CUT=/usr/bin/cut
SSH_KEY=/usr/bin/dropbearkey
UCI=/sbin/uci
TMP_FILE=/tmp/.installer.$$
LOG_FILE=/usr/local/log/install.log
STATUS=/tmp/.installer.api.$$
NEW_CONF_MARK=/etc/first_bizwi_boot
WIFI_CONF=/etc/config/wireless
NET_CONF=/etc/config/network
SPLASH_PAGE=/etc/nodogsplash/htdocs/splash.html
PROC_NAME=`basename $0`"["$$"]"

# global vars.
. /usr/local/etc/bizwi.sh

# execute only if first boot mark, else exit (/etc/first_bizwi_boot exits).
if [ -f ${NEW_CONF_MARK} ]
then
	${LOGGER} -t "${PROC_NAME}" "starting BIZWI SENSOR v"${BIZWI_VERSION}" install..."
else
	${LOGGER} -t "${PROC_NAME}" "already installed."
	exit 1
fi

# set hostname to SID (Sensor ID)
uci set system.@system[0].hostname=${BIZWI_SID}
uci get system.@system[0].hostname > /proc/sys/kernel/hostname

# config config captive portal SID & URL (for nodogsplash)
SPLASH_TEMPLATE=/usr/local/etc/splash.html.conf
${SED} 's/BIZWISID/'${BIZWI_SID}'/g' ${SPLASH_TEMPLATE} | ${SED} 's,BIZWISPLASH,'${BIZWI_SPLASH}',g'  > ${SPLASH_PAGE}

# config SSID
cp /etc/config/wireless ${TMP_FILE}
sed 's/BIZWISSID/'${BIZWI_SSID}'/g' ${TMP_FILE} > /etc/config/wireless
# clean
rm -rf ${TMP_FILE}

# generate & register RSA keys in cloud.
/usr/local/bin/register

# delete first boot mark
${LOGGER} -t "${PROC_NAME}" "deleting first boot mark"
rm -rf ${NEW_CONF_MARK}

# save install log
${LOGGER} -t "${PROC_NAME}" "saving install log to "${LOG_FILE}
${LOGREAD} > ${LOG_FILE}

# reboot
${LOGGER} -t "${PROC_NAME}" "network config changed, rebooting..."
${REBOOT} -f
