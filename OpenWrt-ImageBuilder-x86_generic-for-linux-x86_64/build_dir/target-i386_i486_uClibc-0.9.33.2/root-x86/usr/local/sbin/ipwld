#!/bin/sh
# ipwld - IP white-list (update) daemon
#
# ChangeLog
# v0.7
# - support for nodogsplash automatic firewall rule config & restart.
# - more logging support
# v0.7rc1
# - fixed logging message errors (PROC_NAME is defined).
# - support for bizwi hosts.

LOGGER=/usr/bin/logger
GEN_IPWL=/usr/local/bin/geniplist
TMP1=/tmp/ipwld.1.$$
TMP2=/tmp/ipwld.2.$$
TAG='#INSERTHERE'
RULES=/tmp/rules
HOST_GOOGLE=/usr/local/etc/hosts.google
NETS_GOOGLE=/usr/local/etc/nets.google
HOST_FB=/usr/local/etc/hosts.facebook
NETS_FB=/usr/local/etc/nets.facebook
HOST_BW=/usr/local/etc/hosts.bizwi
NETS_BW=/usr/local/etc/nets.bizwi
NDS_CONF=/etc/nodogsplash/nodogsplash.conf
PROC_NAME=`basename $0`"["$$"]"

# log
${LOGGER} -t ${PROC_NAME} "starting daemon..."

# bizwi: create or update whitelists
${GEN_IPWL} ${HOST_BW} ${NETS_BW}

for NET in `cat ${NETS_BW}`
do
  CHK=`egrep ${NET} ${NDS_CONF}`
  if [ ! "${CHK}" ]
  then
    ${LOGGER} -t ${PROC_NAME} " warning" ${NET} "(bizwi) rule not present, adding it"
    echo "  FirewallRule allow tcp port 80 to" ${NET}"/24" >> ${RULES}
    echo "  FirewallRule allow tcp port 443 to" ${NET}"/24" >> ${RULES}
  fi
done

# google: create or update whitelists
${GEN_IPWL} ${HOST_GOOGLE} ${NETS_GOOGLE}

for NET in `cat ${NETS_GOOGLE}`
do
  CHK=`egrep ${NET} ${NDS_CONF}`
  if [ ! "${CHK}" ]
  then
    ${LOGGER} -t ${PROC_NAME} " warning" ${NET} "(google) rule not present, adding it"
    echo "  FirewallRule allow tcp port 443 to" ${NET}"/24" >> ${RULES}
  fi 
done

# fb: create or update whitelists
${GEN_IPWL} ${HOST_FB} ${NETS_FB}

for NET in `cat ${NETS_FB}`
do
  CHK=`egrep ${NET} ${NDS_CONF}`
  if [ ! "${CHK}" ]
  then
    ${LOGGER} -t ${PROC_NAME} " warning" ${NET} "(facebook) rule not present, adding it"
    echo "  FirewallRule allow tcp port 443 to" ${NET}"/24" >> ${RULES} 
  fi
done 

# Update /etc/nodogsplash/nodogsplash.conf firewall preauth rules
${LOGGER} -t ${PROC_NAME} " updating" ${NDS_CONF} "rules..."

LINEA=`cat $NDS_CONF | grep -n $TAG |cut -d':' -f1`
LINEA_HEAD=`expr $LINEA - 1`
TOTAL=`wc -l $NDS_CONF |cut -d' ' -f1`
LINEA_TAIL=`expr $TOTAL - $LINEA_HEAD`

head -$LINEA_HEAD $NDS_CONF > $TMP1
tail -$LINEA_TAIL $NDS_CONF > $TMP2

cat $RULES >> $TMP1
cat $TMP2 >> $TMP1

mv $TMP1 $NDS_CONF

# clean-up
rm -rf $TMP2
rm -rf ${RULES}

# restart nodogsplash
${LOGGER} -t ${PROC_NAME} " stopping nodogsplash daemon..."
killall nodogsplash
sleep 10
${LOGGER} -t ${PROC_NAME} " starting nodogsplash daemon..."
/usr/local/sbin/nodogsplash -d 5 -s -w /tmp/ndsctl.sock -c /etc/nodogsplash/nodogsplash.conf

