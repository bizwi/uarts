#!/bin/sh -x
# BIZWI Sensor Main Custom Config File
# (c) 2014 jcb/BIZWI
#
# ChangeLog
# v0.7
# - UCI Support for basic bizwi customer data (moved to /etc/config/bizwi)
# - Status delay set to 1min from 5min

WGET=/usr/bin/wget
MD5SUM=/usr/bin/md5sum
CUT=/usr/bin/cut
UCI=/sbin/uci
IFCONFIG=/sbin/ifconfig
GREP=/bin/egrep
AWK=/usr/bin/awk
TR=/usr/bin/tr

# Global variables.
export BIZWI_VERSION="0.7"
if [ ! -f "/tmp/sysinfo/model" ]; then 
        export BIZWI_MODEL='x86_sensorR'

else
export BIZWI_MODEL=`cat /tmp/sysinfo/model`
fi
export BIZWI_MAC=`${IFCONFIG} br-lan | ${GREP} br-lan | ${AWK} '{print $5}' | ${TR} '[A-F]' '[a-f]'`
export BIZWI_SID=`echo ${BIZWI_MAC} | ${MD5SUM} | ${CUT} -d' ' -f1`
export BIZWI_SSID=`uci get bizwi.@wifi[0].ssid`
export BIZWI_USER=`uci get bizwi.@cloud[0].user`
export BIZWI_PASSWD=`uci get bizwi.@cloud[0].passwd`
export BIZWI_IFACE=`${IFCONFIG} | ${GREP} "00\-00\-00" | ${CUT} -d" " -f1`

uci set system.@system[0].hostname=${BIZWI_SID}
uci get system.@system[0].hostname > /proc/sys/kernel/hostname
export BIZWI_HOSTNAME=`uci get system.@system[0].hostname`

export BIZWI_SERVER=`uci get bizwi.@cloud[0].server`
export BIZWI_API_SERVER=`uci get bizwi.@cloud[0].api`
export BIZWI_DATA_SERVER=`uci get bizwi.@cloud[0].data`
export BIZWI_CALLBACK_SERVER=`uci get bizwi.@cloud[0].callback`

export BIZWI_AGENT='bizwi-sensor ('${BIZWI_MODEL}')/'${BIZWI_VERSION}

export BIZWI_RETRY=1
export BIZWI_STATUS_DELAY=300

export BIZWI_STATUS="http://"${BIZWI_API_SERVER}"/api/v1/status/"
export BIZWI_DATA="http://"${BIZWI_API_SERVER}"/api/v1/data/"
export BIZWI_DATA_BULK="http://"${BIZWI_API_SERVER}"/api/v1/databulk/"
export BIZWI_PING="http://"${BIZWI_API_SERVER}"/api/v0/getip/"
export BIZWI_AUTH="http://"${BIZWI_API_SERVER}"/api/v1/assoc/"
export BIZWI_CALLBACK="http://"${BIZWI_API_SERVER}"/api/v0/callback/"
export BIZWI_REGISTER="http://"${BIZWI_API_SERVER}"/api/v0/register/"

export BIZWI_SPLASH="http://"${BIZWI_SERVER}"/profiles/register/"
export BIZWI_ERROR="http://"${BIZWI_SERVER}"/profiles/error/"

export BIZWI_IP_LAN=`${IFCONFIG} br-lan|grep inet| ${AWK} '{printf "%s", substr($2,6)}'`
export BIZWI_IP=`uci get network.wifi.ipaddr`

# geolocalization
JSON_FILE=/tmp/.geoloc.json
if [ -s ${JSON_FILE} ]
then
	export BIZWI_GEO_ACC=`cat ${JSON_FILE} | egrep accura | sed 's/\,//g' | sed 's/\ //g' | cut -d':' -f2`
	export BIZWI_GEO_LAT=`cat ${JSON_FILE} | egrep lat | sed 's/\,//g' | sed 's/\ //g' | cut -d':' -f2`
	export BIZWI_GEO_LNG=`cat ${JSON_FILE} | egrep lng | sed 's/\ //g' | cut -d':' -f2`
else
	export BIZWI_GEO_ACC=0
	export BIZWI_GEO_LAT=0
	export BIZWI_GEO_LNG=0
fi


